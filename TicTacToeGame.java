import java.util.Scanner;
public class TicTacToeGame
{

	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Welcome to the Tic Tac Toe game!");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
		Board board = new Board();
		boolean gameOver = false;
		int player =1;
		Square playerToken = Square.X;
		
		while(!gameOver)
		{
			System.out.println(board);
			System.out.println("Player " + player + ": it's your turn, where would you like to place your token?");
			if(player == 1)
			{
				playerToken = Square.X;
			}
			else
			{
				playerToken = Square.O;
			}
			System.out.println("Which Row?");
			int row = scanner.nextInt();
			System.out.println("Which Column");
			int col = scanner.nextInt();
			while(!board.placeToken(row,col,playerToken))
			{
				System.out.println("INVALID INPUT, RE-ENTER VALUES!");
				System.out.println("Which Row?");
				row = scanner.nextInt();
				System.out.println("Which Column");
				col = scanner.nextInt();
			}
			if(board.checkIfFull())
			{
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else if(board.checkIfWinning(playerToken))
			{
				System.out.println(board);
				System.out.println("Player " + (player )  + " is the winner!");
				gameOver = true;
			}
			else
			{
				player ++;
				if(player == 3)
				{
					player = 1;
				}
			}
		}
		
	}
}