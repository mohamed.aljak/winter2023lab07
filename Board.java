public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		this.tictactoeBoard = new Square[][]{
			{Square.BLANK,Square.BLANK,Square.BLANK},
			{Square.BLANK,Square.BLANK,Square.BLANK},
			{Square.BLANK,Square.BLANK,Square.BLANK}
		};
	}
	public String toString()
	{
		// Horizontal Index
		String table = "   0  1  2" + "\n";
		for(int i = 0; i< this.tictactoeBoard.length; i++)
		{
			//Vertical Index
			table += i ;
			
			for(int j = 0; j < this.tictactoeBoard[i].length; j++)
			{
				table += "  " + this.tictactoeBoard[i][j] ;
			}
			table += "\n";
		}
		return table;
	}
	public boolean placeToken(int row, int col, Square playerToken) 
	{
		if(row < 1 && row > 3 && col < 1 && col > 3)
		{
			return false;
		}
		if(this.tictactoeBoard[row][col] == Square.BLANK)
		{
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		return false;
	}
	public boolean checkIfFull()
	{
		for(int i = 0; i< this.tictactoeBoard.length; i++)
		{
			for(int j = 0; j < this.tictactoeBoard[i].length; j++)
			{
				if(this.tictactoeBoard[i][j] == Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		for(int i = 0; i< this.tictactoeBoard.length; i++)
		{
			if(this.tictactoeBoard[i][0] == playerToken && this.tictactoeBoard[i][1] == playerToken && this.tictactoeBoard[i][2] == playerToken)
			{
				return true;
			}
		}
		return false;
	}
	private boolean checkIfWinningVertical(Square playerToken)
	{
		for(int i = 0; i< this.tictactoeBoard.length; i++)
		{
			if(this.tictactoeBoard[0][i] == playerToken && this.tictactoeBoard[1][i] == playerToken && this.tictactoeBoard[2][i] == playerToken)
			{
				return true;
			}
		}
		return false;
	}
	public boolean checkIfWinning(Square playerToken) 
	{
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken))
		{
			return true;
		}
		return false;
	}	
		public boolean checkIfWinningDiagonal(Square playerToken)
		{
			if(this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[1][1] == playerToken && this.tictactoeBoard[2][2] == playerToken)
			{
				return true;
			}
			if(this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[1][1] == playerToken && this.tictactoeBoard[2][0] == playerToken)
			{
				return true;
			}
			return false;
		}
}