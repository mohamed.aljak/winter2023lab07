public enum Square
{
	X,
	O,
	BLANK;
	
	public String toString()
	{
		if(this == Square.BLANK)
		{
			return "_";
		}
		else if(this == Square.X)
		{
			return "X";
		}
		else
		{
			return "O";
		}
	}
}